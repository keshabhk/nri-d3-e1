package assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Employee{
	public int id;
	public String name;
	public double salary;
	
	public Employee(int id, String name, double salary)
	{
		this.id=id;
		this.salary = salary;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
	
	
}

public class MapDemo {

	public static void main(String args[])
	{
		List<Employee> empdata =  new ArrayList<Employee>();
		empdata.add(new Employee(101,"abc",100000));
		empdata.add(new Employee(102,"def",200000));
		empdata.add(new Employee(103,"ghi",300000));
		empdata.add(new Employee(103,"jkl",400000));
		empdata.add(new Employee(104,"mno",500000));
		
		List<Employee> mappeddata = new ArrayList<Employee>();
		
		//increment salary by 10% using map
		mappeddata =empdata.stream().map(e->new Employee(e.id,e.name,e.salary+0.1*e.salary)).collect(Collectors.toList());
		
		//show updated salary of employees
		for(Employee i:mappeddata)
			System.out.println(i);
		
		
		
	}
}
