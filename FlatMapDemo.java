package assignment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


class Product{
	int id;
	String name;
	double price;
	
	public Product(int id,String name, double price)
	{
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
	
	
}


public class FlatMapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Product> pr1 = Arrays.asList(new Product(101,"hp laptop",45000),new Product(102,"acer laptop",50000),new Product(103,"samsung laptop",60000)); 
		List<Product> pr2 = Arrays.asList(new Product(201,"hp phone",43000),new Product(202,"hp phone",51000),new Product(203,"hp phone",63000));
		List<Product> pr3 = Arrays.asList(new Product(301,"dell camera",47000),new Product(302,"dell camera",50000),new Product(303,"dell camera",61000));
		
		//2D List
		List<List<Product>> products = new ArrayList<List<Product>>();
		products.add(pr1);
		products.add(pr2);
		products.add(pr3);
		
		//flat map.
		List<Product> allproducts = products.stream().flatMap(plist->plist.stream()).collect(Collectors.toList());
		
		for(Product pr: allproducts)
			System.out.println(pr);
		
		
		

	}

}
