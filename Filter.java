package assignment;
import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;

public class Filter {
 public static void main(String args[])
 {
	 List<String> words = new ArrayList<String>();
	 words.add("abcdef");
	 words.add("asdfghjk");
	 words.add("abc");
	 words.add("lkjgafagaa");
	 words.add("asdfg");
	 words.add("poiuytr");
	 
	 //using stream API to filter all names with length > 5 and <8.
	 List<String> filtered_words = new ArrayList<String>();
	 filtered_words = words.stream().filter(w->w.length()>5 && w.length()<8).collect(Collectors.toList());
	 System.out.println(filtered_words);
	 
 }
}
